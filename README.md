# mariadb-backup.sh
a simple script to dump and rotate MariaDB backups

## Usage

This script can be used to dump a MariaDB (or MySQL) database to disk and to rotate through a given number of backups. It supports two command line arguments:

* **-q** run silently (for use in crontabs or other non-interactive settings)
* **-k *n*** number of recent backups to keep (default is 7)
 
The script assumes that backup user has a ~/.my.cnf file storing the database connection (or passwordless access to the database as the current userid). It's a good idea to create a MariaDB user that has the appropriate permissions for dumping databases instead of running this script as the unix root user. 

## Configuration variables and options

```bash
## set default variables
backup_root=/srv/backups/mariadb        # root directory for all backups
v=true                                  # verbose output
keep=7                                  # number of old backups to keep
hash=sha256                             # crypto hash to use for checksums

# set mysqldump options
dumpopts='--single-transaction --flush-logs --flush-privileges'

## don't edit below this line

# get our options
while getopts qk:h: opt; do
  case $opt in
  q)
      v=false
      ;;
  k)
      keep=$OPTARG
      ;;
  h)
      hash=$OPTARG
      ;;
  esac
done
shift $((OPTIND - 1))
```
